﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NewsReader.DataClasses;

namespace ReutersReader
{
    /// <summary>
    /// Since Reuters ended their own RSS feed earlier in 2020, I cheat with a google source instead.
    /// </summary>
    public class ReutersReaderFactory : IReaderFactory
    {
        public async Task<IEnumerable<IReader>> GetReaders()
        {
            var reutersReader = new ReutersByGoogleReader();
            await reutersReader.Init();

            return new List<IReader> { reutersReader };
        }
    }
}
