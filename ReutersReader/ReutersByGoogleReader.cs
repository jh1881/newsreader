﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeHollow.FeedReader;
using NewsReader.DataClasses;

namespace ReutersReader
{
    public class ReutersByGoogleReader : IReader
    {
        public string Name => "Reuters (by Google)";
        public string Description { get; private set; }
        public string ImageUrl { get; private set; }
        public bool Valid { get; private set; }
        public string Id { get; } = "{22223104-ECBA-FFDA-1243-1E39265FA7C5}";

        private static string SourceUrl => "https://news.google.com/rss/search?q=when:24h+allinurl:reuters.com&ceid=US:en&hl=en-US&gl=US";

        public async Task<IEnumerable<Article>> GetArticles()
        {
            var feed = await FeedReader.ReadAsync(SourceUrl);
            return feed.Items.Select(i => new Article
            {
                Title = i.Title,
                Description = i.Description,
                Link = i.Link,
                PublishedDate = i.PublishingDate,
                Id = i.Id
            });
        }

        public async Task Init()
        {
            try
            {
                var feed = await FeedReader.ReadAsync(SourceUrl);
                Description = feed.Description;
                ImageUrl = feed.ImageUrl;
                Valid = true;
            }
            catch
            {
                Valid = false;
            }
        }
    }
}
