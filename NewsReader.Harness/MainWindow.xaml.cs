﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using BBCReader;
using GuardianReader;
using NewsReader.DataClasses;
using OtherReaders;
using ReutersReader;

namespace NewsReader.Harness
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            LoadReaders();
        }

        private async Task LoadReaders()
        {
            try
            {
                // It would be better to search for IReaderFactory implementations here.
                var factories = new List<IReaderFactory> { new BbcReaderFactory(), new ReutersReaderFactory(), new GuardianReaderFactory(), new OtherReadersFactory() };
                // get the readers.
                var readers = new List<IReader>();
                foreach (var factory in factories)
                {
                    var factoryReaders = await factory.GetReaders();
                    readers.AddRange(factoryReaders);
                }

                var minDate = DateTime.MinValue;
                foreach (var reader in readers)
                {
                    var articles = await reader.GetArticles();
                    var thisMinDate = articles.Min(a => a.PublishedDate ?? DateTime.MinValue);
                    if (minDate < thisMinDate) minDate = thisMinDate;
                }
            }
            catch (Exception ee)
            {
                throw;
            }
        }
    }
}
