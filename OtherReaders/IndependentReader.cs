﻿namespace OtherReaders
{
    class IndependentReader : BaseReader
    {
        public IndependentReader()
        {
            SourceUrl = "https://www.independent.co.uk/rss";
            Id = "{A670AE1C-ED31-46B1-BFC9-E17403140988}";
        }
    }
}
