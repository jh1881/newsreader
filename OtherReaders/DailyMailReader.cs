﻿namespace OtherReaders
{
    class DailyMailReader : BaseReader
    {
        public DailyMailReader()
        {
            SourceUrl = "https://www.dailymail.co.uk/home/index.rss";
            Id = "{A670AE1C-ED31-46B1-BFC9-E17403140987}";
        }
    }
}
