﻿namespace OtherReaders
{
    class PinkNewsReader : BaseReader
    {
        public PinkNewsReader()
        {
            SourceUrl = "https://www.pinknews.co.uk/feed/";
            Id = "{A670AE1C-ED31-46B1-BFC9-E17403140989}";
        }
    }
}
