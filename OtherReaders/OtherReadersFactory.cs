﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using NewsReader.DataClasses;

namespace OtherReaders
{
    public class OtherReadersFactory : IReaderFactory
    {
        public async Task<IEnumerable<IReader>> GetReaders()
        {
            var typeBaseReader = typeof(BaseReader);
            var readers = new List<BaseReader>();
            foreach (var type in Assembly.GetAssembly(typeof(OtherReadersFactory)).GetTypes())
            {
                if (!type.IsAbstract && typeBaseReader.IsAssignableFrom(type))
                {
                    var reader = (BaseReader)Activator.CreateInstance(type);
                    await reader.Init();
                    readers.Add(reader);
                }
            }

            return readers;
        }
    }
}
