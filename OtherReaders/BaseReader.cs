﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeHollow.FeedReader;
using NewsReader.DataClasses;

namespace OtherReaders
{
    public abstract class BaseReader : IReader
    {
        public string Name { get; protected set; }
        public string Description { get; protected set; }
        public string ImageUrl { get; protected set; }
        public bool Valid { get; protected set; }
        public string Id { get; protected set; }
        protected string SourceUrl { get; set; }

        public virtual async Task<IEnumerable<Article>> GetArticles()
        {
            var feed = await FeedReader.ReadAsync(SourceUrl);
            return feed.Items.Select(i => new Article
            {
                Title = i.Title,
                Description = i.Description,
                Link = i.Link,
                PublishedDate = i.PublishingDate,
                Id = i.Id
            });
        }

        public virtual async Task Init()
        {
            try
            {
                var feed = await FeedReader.ReadAsync(SourceUrl);
                Name = feed.Title;
                Description = feed.Description;
                ImageUrl = feed.ImageUrl;
                Valid = true;
            }
            catch
            {
                Valid = false;
            }
        }
    }
}
