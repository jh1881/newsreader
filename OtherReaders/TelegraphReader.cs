﻿namespace OtherReaders
{
    class TelegraphReader : BaseReader
    {
        public TelegraphReader()
        {
            SourceUrl = "https://www.telegraph.co.uk/rss.xml";
            Id = "{A670AE1C-ED31-46B1-BFC9-E1740314098A}";
        }
    }
}
