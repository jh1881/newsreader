﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeHollow.FeedReader;
using NewsReader.DataClasses;

namespace GuardianReader
{
    public class GuardianReader : IReader
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string ImageUrl { get; private set; }
        public bool Valid { get; private set; }
        public string Id { get; } = "{1FC03106-4240-12B7-9C94-1BC0BB36E0C5}";

        private static string SourceUrl => "https://www.theguardian.com/uk/rss";
        
        public async Task Init()
        {
            try
            {
                var feed = await FeedReader.ReadAsync(SourceUrl);
                Name = feed.Title;
                Description = feed.Description;
                ImageUrl = feed.ImageUrl;
                Valid = true;
            }
            catch
            {
                Valid = false;
            }
        }

        public async Task<IEnumerable<Article>> GetArticles()
        {
            var feed = await FeedReader.ReadAsync(SourceUrl);
            return feed.Items.Select(i => new Article
            {
                Title = i.Title,
                Description = i.Description,
                Link = i.Link,
                PublishedDate = i.PublishingDate,
                Id = i.Id
            });
        }
    }
}
