﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NewsReader.DataClasses;

namespace GuardianReader
{
    public class GuardianReaderFactory : IReaderFactory
    {
        public async Task<IEnumerable<IReader>> GetReaders()
        {
            var reader = new GuardianReader();
            await reader.Init();

            return new List<IReader> { reader };
        }
    }
}
