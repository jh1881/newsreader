﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewsReader.DataClasses
{
    /// <summary>
    /// Each available IReader provides a list of articles to be used as a News Source.
    /// </summary>
    public interface IReader
    {
        string Name { get; }
        string Description { get; }
        string ImageUrl { get; }
        bool Valid { get; }
        string Id { get; }

        Task<IEnumerable<Article>> GetArticles();
    }
}
