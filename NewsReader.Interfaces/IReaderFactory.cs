﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewsReader.DataClasses
{
    /// <summary>
    /// The IReaderFactory presents a list of IReader objects that can each be used as a single source of News articles.
    /// </summary>
    public interface IReaderFactory
    {
        Task<IEnumerable<IReader>> GetReaders();
    }
}
