﻿using System;

namespace NewsReader.DataClasses
{
    public class Article
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? PublishedDate { get; set; }
        public string Link { get; set; }
        public string ImageUrl { get; set; }
        public string Id { get; set; }
    }
}
