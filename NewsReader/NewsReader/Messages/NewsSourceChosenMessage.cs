﻿using NewsReader.DataClasses;

namespace NewsReader.Messages
{
    class NewsSourceChosenMessage
    {
        public IReader NewsReader { get; }

        public NewsSourceChosenMessage(IReader newsReader)
        {
            NewsReader = newsReader;
        }
    }
}
