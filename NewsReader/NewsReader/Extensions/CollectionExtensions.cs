﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace NewsReader.Extensions
{
    public static class CollectionExtensions
    {
        public static void AddRange<T>(this ObservableCollection<T> coll, IEnumerable<T> items)
        {
            foreach(var i in items) coll.Add(i);
        }
    }
}
