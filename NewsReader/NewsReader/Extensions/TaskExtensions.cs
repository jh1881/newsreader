﻿using System.Threading.Tasks;

namespace NewsReader.Extensions
{
    public static class TaskExtensions
    {
        public static void FireAndForget(this Task task)
        {
            Task.Run(task.Wait);
        }
    }
}
