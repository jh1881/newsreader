﻿using System.Reflection;
using Xamarin.Forms;
using NewsReader.Services;
using NewsReader.ViewModels;
using TinyIoC;

namespace NewsReader
{
    public partial class App : Application, IMainPage
    {
        public new static App Current => (App) Application.Current;
        public TinyIoCContainer Container { get; } = TinyIoCContainer.Current;

        public App()
        {
            InitializeComponent();
            var viewLocator = new ViewLocator();
            // The viewLocator.Init() will register all the view models.
            viewLocator.Init(Assembly.GetAssembly(typeof(App)));
            Container.Register<IViewLocator>(viewLocator);
            Container.Register<INavigationService>(new NavigationService(this, viewLocator));
            Container.Register<IFactorySource, FactorySource>();

            Container.Resolve<INavigationService>().PresentAsNavigatableMainPage<MainViewModel>();
        }
    }
}
