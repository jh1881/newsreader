﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NewsReader.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChooseNewsSourceView : ContentPage
    {
        public ChooseNewsSourceView()
        {
            InitializeComponent();
        }
    }
}