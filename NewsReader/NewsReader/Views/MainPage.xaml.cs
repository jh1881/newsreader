﻿using System;
using System.ComponentModel;
using NewsReader.Services;
using NewsReader.ViewModels;

namespace NewsReader.Views
{
    [DesignTimeVisible(false)]
    public partial class MainPage
    {
        private readonly ChooseNewsSourceViewModel _masterViewModel;

        public MainPage()
        {
            InitializeComponent();

            // In MasterDetailPage, neither Master or Detail can be Binding :(
            _masterViewModel = App.Current.Container.Resolve<ChooseNewsSourceViewModel>();
            var viewLocator = App.Current.Container.Resolve<IViewLocator>();
            Master = viewLocator.CreateAndBindPageFor(_masterViewModel);
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await _masterViewModel.OnShowing();
        }

        private void MainPage_OnIsPresentedChanged(object sender, EventArgs e)
        {
            
        }
    }
}
