﻿using System.Threading.Tasks;
using NewsReader.DataClasses;

namespace NewsReader.ViewModels
{
    class ArticleViewModel : BaseViewModel
    {
        private Article _article;

        public Article Article
        {
            get => _article;
            set => SetProperty(ref _article, value);
        }

        public override void Init(object data)
        {
            if (data is Article article)
            {
                Article = article;
            }
        }
    }
}
