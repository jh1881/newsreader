﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using NewsReader.DataClasses;
using NewsReader.Extensions;
using NewsReader.Helpers;
using NewsReader.Messages;
using NewsReader.Services;
using Xamarin.Forms;

namespace NewsReader.ViewModels
{
    class ChooseNewsSourceViewModel : BaseViewModel
    {
        private readonly IFactorySource _factorySource;
        private readonly List<IReaderFactory> _factories = new List<IReaderFactory>();
        private ICommand _refreshCommand;
        private ICommand _sourceSelectedCommand;

        public ObservableCollection<IReader> Readers { get; set; } = new ObservableCollection<IReader>();

        public ICommand RefreshCommand => _refreshCommand ?? (_refreshCommand = new Command(async () => await LoadSources()));
        public ICommand SourceSelectedCommand => _sourceSelectedCommand ?? (_sourceSelectedCommand = new Command<IReader>(reader => SwitchNewsSource(reader)));

        public ChooseNewsSourceViewModel(IFactorySource factorySource)
        {
            _factorySource = factorySource;
            Title = "Available News Sources";
        }

        public override async Task OnShowing() => await LoadSources();

        private void SwitchNewsSource(IReader reader)
        {
            if (reader == null) return;
            UserSettings.LastSourceId = reader.Id;
            MessagingCenter.Send(new NewsSourceChosenMessage(reader), string.Empty);
        }

        public async Task LoadSources()
        {
            try
            {
                IsBusy = true;

                var factories = _factorySource.Factories;
                // Now get the readers and order by name.
                var readers = new List<IReader>();
                foreach (var factory in factories)
                {
                    var factReaders = await factory.GetReaders();
                    readers.AddRange(factReaders.Where(r => r.Valid));
                }
                readers.Sort((x, y) => string.CompareOrdinal(x.Name, y.Name));

                // Now push this into the Readers
                Readers.Clear();
                Readers.AddRange(readers);
            }
            catch (Exception ee)
            {
                await UserDialogs.Instance.AlertAsync("There was an error getting news sources.", "News Reader");
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
