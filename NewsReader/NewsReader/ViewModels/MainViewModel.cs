﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using NewsReader.DataClasses;
using NewsReader.Extensions;
using NewsReader.Helpers;
using NewsReader.Messages;
using NewsReader.Services;
using Xamarin.Forms;

namespace NewsReader.ViewModels
{
    class MainViewModel : BaseViewModel
    {
        private readonly INavigationService _navigationService;
        private readonly IFactorySource _factorySource;
        private bool _menuIsOpen;
        private BaseViewModel _masterViewModel;
        private BaseViewModel _detailViewModel;
        private IReader _currentReader;
        private ICommand _refreshCommand;
        private ICommand _articleTappedCommand;
        private TextType _textType;
        public ICommand RefreshCommand => _refreshCommand ?? (_refreshCommand = new Command(async () => await LoadArticles()));
        public ICommand ArticleTappedCommand => _articleTappedCommand ?? (_articleTappedCommand = new Command<Article>(ArticleChanged));
        public ObservableCollection<Article> Articles { get; } = new ObservableCollection<Article>();

        public IReader CurrentReader
        {
            get => _currentReader;
            set => SetProperty(ref _currentReader, value, onChanged: async () => await LoadArticles());
        }

        public bool MenuIsOpen
        {
            get => _menuIsOpen;
            set => SetProperty(ref _menuIsOpen, value);
        }

        public BaseViewModel MasterViewModel
        {
            get => _masterViewModel;
            set => SetProperty(ref _masterViewModel, value);
        }

        public BaseViewModel DetailViewModel
        {
            get => _detailViewModel;
            set => SetProperty(ref _detailViewModel, value);
        }

        public TextType TextType
        {
            get => _textType;
            set => SetProperty(ref _textType, value);
        }

        public MainViewModel(INavigationService navigationService, IFactorySource factorySource)
        {
            _navigationService = navigationService;
            _factorySource = factorySource;
            MasterViewModel = App.Current.Container.Resolve<ChooseNewsSourceViewModel>();

            MessagingCenter.Subscribe<NewsSourceChosenMessage>(this, string.Empty, msg =>
            {
                MenuIsOpen = false;
                CurrentReader = msg.NewsReader;
            });
        }

        public override async Task OnShowing()
        {
            if (UserSettings.LastSourceId != null)
            {
                IsBusy = true;
                try
                {
                    // Get an object for this and show it!

                    foreach (var factory in _factorySource.Factories)
                    {
                        var readers = await factory.GetReaders();
                        var chosenReader = readers.FirstOrDefault(f => f.Id == UserSettings.LastSourceId);
                        if (chosenReader != null)
                        {
                            CurrentReader = chosenReader;
                            return;
                        }
                    }
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        private void ArticleChanged(Article article)
        {
            // When article is selected (ie tapped) display a new view!
            _navigationService.NavigateTo<ArticleViewModel>(article);
        }

        private async Task LoadArticles()
        {
            IsBusy = true;
            try
            {
                Articles.Clear();
                if (CurrentReader == null) return;
                var articles = (await CurrentReader.GetArticles()).ToList();
                Articles.AddRange(articles.OrderByDescending(a => a.PublishedDate).ThenBy(a => a.Title));

                // quick bit of defensive code to avoid a crash if the Description doesnt exist.
                if (articles.Any(a => ContainsHtmlRegex.IsMatch(a.Description ?? "")))
                {
                    TextType = TextType.Html;
                }
                else TextType = TextType.Text;
            }
            finally
            {
                IsBusy = false;
            }
        }

        private static readonly Regex ContainsHtmlRegex = new Regex(@"<\s*([^ >]+)[^>]*>.*?<\s*/\s*\1\s*>");
    }
}
