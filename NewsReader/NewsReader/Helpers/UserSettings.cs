﻿using Xamarin.Essentials;

namespace NewsReader.Helpers
{
    public static class UserSettings
    {
        public static string LastSourceId
        {
            get => Preferences.Get("LastSourceId", null);
            set => Preferences.Set("LastSourceId", value);
        }
    }
}
