﻿using System;
using System.Collections.Generic;
using System.Reflection;
using NewsReader.ViewModels;
using Xamarin.Forms;

namespace NewsReader.Services
{
    class ViewLocator : IViewLocator
    {
        // keep a cache
        private readonly Dictionary<Type, Type> _vmToPageDictionary = new Dictionary<Type, Type>();

        public Page CreateAndBindPageFor<TViewModel>(TViewModel viewModel) where TViewModel : BaseViewModel
        {
            var pageType = FindPageForViewModel(viewModel.GetType());
            var page = (Page)Activator.CreateInstance(pageType);
            page.BindingContext = viewModel;
            return page;
        }

        protected virtual Type FindPageForViewModel(Type viewModelType)
        {
            if (_vmToPageDictionary.TryGetValue(viewModelType, out var type)) return type;
            return null;
        }

        private static Type FindTypeReplacement(Type viewModelType, string from, string to)
        {
            var name = viewModelType.AssemblyQualifiedName.Replace(".ViewModels", ".Views");
            var pageTypeName = name.Replace(from, to);
            return Type.GetType(pageTypeName);
        }

        public void Init(Assembly viemModelAssembly)
        {
            var baseType = typeof(BaseViewModel);
            var assemblyTypes = viemModelAssembly.GetTypes();

            foreach (var type in assemblyTypes)
            {
                if (type.IsInterface || type.IsAbstract || !baseType.IsAssignableFrom(type)) continue;
                var pageType = FindTypeReplacement(type, "ViewModel", "View")
                               ?? FindTypeReplacement(type, "ViewModel", "Page")
                               ?? FindTypeReplacement(type, "PageModel", "View")
                               ?? FindTypeReplacement(type, "PageModel", "Page");
                if (pageType == null) continue;

                App.Current.Container.Register(type);
                _vmToPageDictionary.Add(type, pageType);
            }
        }
    }
}
