﻿using NewsReader.ViewModels;
using Xamarin.Forms;

namespace NewsReader.Services
{
    public interface IViewLocator
    {
        Page CreateAndBindPageFor<T>(T viewModel) where T : BaseViewModel;
    }
}
