﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NewsReader.Extensions;
using NewsReader.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace NewsReader.Services
{
    public class NavigationService : INavigationService
    {
        private readonly IMainPage _presentationRoot;
        private readonly IViewLocator _viewLocator;

        private INavigation Navigator
        {
            get
            {
                if (_presentationRoot.MainPage is MasterDetailPage mdPage)
                {
                    return mdPage.Detail.Navigation;
                }
                return _presentationRoot.MainPage.Navigation;
            }
        }

        public NavigationService(IMainPage presentationRoot, IViewLocator viewLocator)
        {
            _presentationRoot = presentationRoot;
            _viewLocator = viewLocator;
        }

        public void PresentAsMainPage(BaseViewModel viewModel, object data = null)
        {
            viewModel.Init(data);
            var page = _viewLocator.CreateAndBindPageFor(viewModel);
            var viewModelsToDismiss = FindViewModelsToDismiss(_presentationRoot.MainPage);

            if (_presentationRoot.MainPage is NavigationPage navPage)
            {
                // If we're replacing a navigation page, unsub from events
                navPage.PopRequested -= NavPagePopRequested;
            }

            page.Appearing += PageOnAppearing;
            _presentationRoot.MainPage = page;
            foreach (var toDismiss in viewModelsToDismiss)
                toDismiss.AfterDismissed();
        }

        private static async void PageOnAppearing(object sender, EventArgs e)
        {
            var page = (Page) sender;
            page.Appearing -= PageOnAppearing;
            await ((BaseViewModel) page.BindingContext).OnShowing();
        }

        public void PresentAsMainPage<T>(object data = null) where T : BaseViewModel
        {
            // Create the view model from IoC.
            var vm = (BaseViewModel)App.Current.Container.Resolve<T>();
            PresentAsMainPage(vm, data);
        }

        public void PresentAsNavigatableMainPage(BaseViewModel viewModel, object data = null)
        {
            viewModel.Init(data);
            var page = _viewLocator.CreateAndBindPageFor(viewModel);
            var newNavigationPage = new NavigationPage(page);
            var viewModelsToDismiss = FindViewModelsToDismiss(_presentationRoot.MainPage);

            if (_presentationRoot.MainPage is NavigationPage navPage)
                navPage.PopRequested -= NavPagePopRequested;

            page.Appearing += PageOnAppearing;

            // Listen for back button presses on the new navigation bar
            newNavigationPage.PopRequested += NavPagePopRequested;
            _presentationRoot.MainPage = newNavigationPage;

            foreach (var toDismiss in viewModelsToDismiss)
                toDismiss.AfterDismissed();
        }

        public void PresentAsNavigatableMainPage<T>(object data = null) where T : BaseViewModel
        {
            // Create the view model from IoC.
            var vm = (BaseViewModel)App.Current.Container.Resolve<T>();
            PresentAsNavigatableMainPage(vm, data);
        }

        private IEnumerable<BaseViewModel> FindViewModelsToDismiss(Page dismissingPage)
        {
            var viewmodels = new List<BaseViewModel>();

            if (dismissingPage is NavigationPage)
            {
                viewmodels.AddRange(Navigator
                    .NavigationStack
                    .Select(p => p.BindingContext)
                    .OfType<BaseViewModel>());
            }
            else
            {
                if (dismissingPage?.BindingContext is BaseViewModel viewmodel) viewmodels.Add(viewmodel);
            }

            return viewmodels;
        }

        private void NavPagePopRequested(object sender, NavigationRequestedEventArgs e)
        {
            if (Navigator.NavigationStack.LastOrDefault()?.BindingContext is BaseViewModel poppingPage)
            {
                poppingPage.AfterDismissed();
            }
        }

        public async Task NavigateTo(BaseViewModel viewModel, object data = null)
        {
            viewModel.Init(data);
            var page = _viewLocator.CreateAndBindPageFor(viewModel);
            page.Appearing += PageOnAppearing;

            await Navigator.PushAsync(page);
        }

        public Task NavigateTo<T>(object data = null) where T : BaseViewModel
        {
            // Create the view model from IoC.
            var vm = (BaseViewModel)App.Current.Container.Resolve<T>();
            return NavigateTo(vm, data);
        }

        public async Task NavigateBack()
        {
            var dismissing = Navigator.NavigationStack.Last().BindingContext as BaseViewModel;
            await Navigator.PopAsync();
            dismissing?.AfterDismissed();
        }

        public async Task NavigateBackToRoot()
        {
            var toDismiss = Navigator
                .NavigationStack
                .Skip(1)
                .Select(vw => vw.BindingContext)
                .OfType<BaseViewModel>()
                .ToArray();

            await Navigator.PopToRootAsync();

            foreach (var viewModel in toDismiss)
            {
                viewModel.AfterDismissed().FireAndForget();
            }
        }
    }
}
