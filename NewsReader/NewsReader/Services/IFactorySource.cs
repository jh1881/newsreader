﻿using System.Collections.Generic;
using NewsReader.DataClasses;

namespace NewsReader.Services
{
    interface IFactorySource
    {
        IEnumerable<IReaderFactory> Factories { get; }
    }
}
