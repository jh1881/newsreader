﻿using System.Collections.Generic;
using BBCReader;
using GuardianReader;
using NewsReader.DataClasses;
using OtherReaders;
using ReutersReader;

namespace NewsReader.Services
{
    class FactorySource : IFactorySource
    {
        public IEnumerable<IReaderFactory> Factories { get; } = new List<IReaderFactory>
        {
            new BbcReaderFactory(), new ReutersReaderFactory(), new GuardianReaderFactory(), new OtherReadersFactory()
        };
    }
}
