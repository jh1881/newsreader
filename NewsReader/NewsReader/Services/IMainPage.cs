﻿using Xamarin.Forms;

namespace NewsReader.Services
{
    public interface IMainPage
    {
        Page MainPage { get; set; }
    }
}