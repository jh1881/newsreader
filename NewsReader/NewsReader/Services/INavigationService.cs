﻿using System.Threading.Tasks;
using NewsReader.ViewModels;

namespace NewsReader.Services
{
    public interface INavigationService
    {
        void PresentAsMainPage(BaseViewModel viewModel, object data = null);
        void PresentAsNavigatableMainPage(BaseViewModel viewModel, object data = null);
        void PresentAsMainPage<T>(object data = null) where T : BaseViewModel;
        void PresentAsNavigatableMainPage<T>(object data = null) where T : BaseViewModel;
        Task NavigateTo(BaseViewModel viewModel, object data = null);
        Task NavigateTo<T>(object data = null) where T : BaseViewModel;
        Task NavigateBack();
        Task NavigateBackToRoot();
    }
}
