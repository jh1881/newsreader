﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NewsReader.DataClasses;

namespace BBCReader
{
    public class BbcReaderFactory : IReaderFactory
    {
        public async Task<IEnumerable<IReader>> GetReaders()
        {
            var bbcFullReader = new BbcFullReader();
            var bbcTechReader = new BbcTechnologyReader();
            await bbcFullReader.Init();
            await bbcTechReader.Init();

            return new List<IReader> { bbcFullReader, bbcTechReader };
        }
    }
}
