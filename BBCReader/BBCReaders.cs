﻿using System;

namespace BBCReader
{
    public class BbcFullReader : BaseReader
    {
        public BbcFullReader()
        {
            SourceUrl = "https://feeds.bbci.co.uk/news/uk/rss.xml";
            Id = "{11CE3104-2241-49C9-AE94-1E37AE36D0C5}";
        }
    }

    public class BbcTechnologyReader : BaseReader
    {
        public BbcTechnologyReader()
        {
            SourceUrl = "https://feeds.bbci.co.uk/news/technology/rss.xml";
            Id = "{11CE3104-2241-49C9-AE94-1E37AE36D0C4}";
        }
    }
}
