﻿using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using ReutersReader;

namespace ReaderTests
{
    class ReutersReaderTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task ReutersReaderFactory_ReturnsOneReaderOfCorrectType()
        {
            var factory = new ReutersReaderFactory();

            var readers = await factory.GetReaders();

            Assert.AreEqual(1, readers.Count());

            if (!readers.OfType<ReutersByGoogleReader>().Any())
                Assert.Fail("ReutersByGoogleReader not returned");
        }

        [Test]
        public async Task ReutersByGoogleReader_Inits()
        {
            var reader = new ReutersByGoogleReader();

            await reader.Init();

            Assert.IsTrue(reader.Valid);
            Assert.IsNotNull(reader.Name);
            Assert.IsNotNull(reader.Description);
        }

        [Test]
        public async Task ReutersByGoogleReader_GetsArticles()
        {
            // Note: Init() is not needed to be called to get articles.
            var reader = new ReutersByGoogleReader();

            var articles = await reader.GetArticles();

            CollectionAssert.IsNotEmpty(articles);
        }
    }
}
