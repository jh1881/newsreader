﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBCReader;
using NUnit.Framework;

namespace ReaderTests
{
    class BbcReaderTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task BbcReaderFactory_ReturnsTwoReadersOfCorrectType()
        {
            var factory = new BbcReaderFactory();

            var readers = await factory.GetReaders();

            Assert.AreEqual(2, readers.Count());
            
            if(!readers.OfType<BbcFullReader>().Any())
                Assert.Fail("BBCFullReader not returned");
            if (!readers.OfType<BbcTechnologyReader>().Any())
                Assert.Fail("BbcTechnologyReader not returned");
        }

        [Test]
        public async Task BbcFullReader_Inits()
        {
            var reader = new BbcFullReader();

            await reader.Init();

            Assert.IsTrue(reader.Valid);
            Assert.IsNotNull(reader.Name);
            Assert.IsNotNull(reader.Description);
        }

        [Test]
        public async Task BbcFullReader_GetsArticles()
        {
            // Note: Init() is not needed to be called to get articles.
            var reader = new BbcFullReader();

            var articles = await reader.GetArticles();

            CollectionAssert.IsNotEmpty(articles);
        }
    }
}
